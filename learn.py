# %% [markdown]
# # 強化学習
#
# ## 概要
#
# 1. 五目並べのAI同士の対戦によって教師データを生成します。
# 1. 生成された教師データを回転、反転し、盤面数を増やします。
# 1. すべての盤面に報酬（評価値）を与えます。
# 1. 教師データによってCNNを最適化し、AIを更新します。
# 1. 再び対戦し、1〜5を繰り返します

# %%
import numpy as np
import random as rd
import copy
from time import time
from gomoku import *
from value_network import *
from ai import *
import json


# %%
size = 9  # ボードサイズ
n_learn = 1000  # 学習サイクル数
n_epoch = 5  # バッチごとの学習数
batchsize = 128  # ミニバッチサイズ
gamenumber = 256  # 学習サイクルごとの対戦数
rate = 1e-4  # 学習率
model = value_network_model(size, rate)  # モデル


# %%
# 学習
for learn in range(n_learn):
    x_train = []  # 教師データ（盤面）の格納先
    w_train = []  # 教師データ（手番）の格納先
    t_train = []  # 教師データ（報酬）の格納先
    start = time()  # 開始時間
    print('learn %d' % learn)

    # 自己対戦で教師データを作成
    for i in range(gamenumber):
        g = Game()  # 新しい五目並べゲーム
        g_history = []  # 棋譜の格納先
        win = 0  # 勝者

        # 一回の対戦
        for i in range(size*size):
            # g.rand_put()  # ランダムに石を置く
            g = ai_put(g, model, random=0.2)  # AIで石を置く
            win = g.end_game()  # ゲームの終了判定
            g_history.append(copy.deepcopy(g))
            if win != 0:
                break  # ゲームが終了していればループから出る

        g_temp = copy.deepcopy(g_history[:])

        # 回転、反射対象な盤面を生成
        for _ in range(3):
            for g_h in g_temp:
                g_h.rotate()
                g_history.append(copy.deepcopy(g_h))
        for g_h in g_temp:
            g_h.reflect()
        for _ in range(4):
            for g_h in g_temp:
                g_history.append(copy.deepcopy(g_h))
                g_h.rotate()

        # 学習ラベルを生成
        for g_h in g_history:
            # 報酬（黒勝利：1.0, 白勝利：-1.0, 引き分け：0）
            q_value = [0.0] if win is 0 else [1.0] if win is 1 else [-1.0]
            x_train.append(g_h.square)
            w_train.append([g_h.turn])
            t_train.append(q_value)

    ave_loss = 0
    # 最適化(学習)
    print('boards: ', len(x_train))
    for epoch in range(n_epoch):
        print('epoch %d | ' % epoch, end='')
        # 0~len(x_train)-1の自然数をランダムに並べ替えたリストを生成
        perm = np.random.permutation(len(x_train))
        loss = 0
        for i in range(0, len(x_train), batchsize):
            x_batch = [x_train[j] for j in perm[i:i+batchsize]]
            w_batch = [w_train[j] for j in perm[i:i+batchsize]]
            t_batch = [t_train[j] for j in perm[i:i+batchsize]]
            # loss.append(0.1)
            loss += model.optimize(x_batch, w_batch, t_batch)
        loss /= int(len(x_train) / batchsize)
        print("loss {0:.3f}".format(loss))
        ave_loss += loss
    ave_loss /= n_epoch
    end = time()
    print("time:{0:.1f}".format(end - start))


# %%
