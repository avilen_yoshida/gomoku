
# %%
import numpy as np
import random as rd
import copy
from time import time
from gomoku import *
from value_network import *
from ai import *


# %%
size = 9
model = value_network_model()


# %%
g = Game()
for i in range(size*size):
    ep = 0
    value_out(g, model)
    start = time()
    g = ai_put(g, model, random=0.02)
    end = time()
    if g.end_game() != 0:
        break
value_out(g, model)
eg = g.end_game()
result = 'DRAW' if eg is 0 else 'BLACK WIN' if eg is 1 else 'WHITE WIN'
print(result)


# %%
